#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <stdlib.h>

static long SimuPulse(aSubRecord *precord)
{

    float noise=*(float *)precord->a;
    float height=*(float *)precord->b;
    float width=*(float *)precord->c;
    float pretrig=*(float *)precord->d;
    float nsamples=*(float *)precord->e;
    float pente=*(float *)precord->f;
    pretrig=pretrig*(-1);
    int size = nsamples;
    int i=0;
    for ( i = 0; i < size; i++) {
      float noiseF = (float)rand()/(float)(RAND_MAX/noise);
      float n=noiseF-noise/2;
      float facteur=height/pente;
      if (i>pretrig && i<pretrig+width)
      {
        if (i<pretrig+pente){

               ((float *)precord->vala)[i]=(i-pretrig)*facteur;
        }else if (i>pretrig+width-pente)
        {
               ((float *)precord->vala)[i]=((pretrig+width)-i)*facteur;
        }
        else{
               ((float *)precord->vala)[i]=height+n;
        }
      }else {
                ((float *)precord->vala)[i]=n;
      }
      
      
    }
 //   printf("nsamples %f\n",nsamples);
    ((float *)precord->valb)[0]=nsamples;
  return 0;
}
epicsRegisterFunction(SimuPulse);
